import Provider from './provider';

export default {
    Provider,
    code: { fontSize: 'inherit' },
    pre: { fontSize: '24px' }
};