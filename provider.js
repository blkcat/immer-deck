import React from 'react';

const SDGLogo = () => (
    <div style={{
        position: 'fixed',
        bottom: 50,
        right: 50,
        width: 100,
        height: 100,
        background: 'url("static/logo_sdg.svg") center / contain no-repeat',
        opacity: .25
    }}></div>
);

export const Provider = ({ mode, modes, children }) => {
    const logo = mode === modes.NORMAL
        ? <SDGLogo></SDGLogo>
        : null;

    return (
        <div>
            {logo}

            {children}
        </div>
    );
}

export default Provider;